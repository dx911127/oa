package com.dx.oa.framework.util;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.*;
/**
 * 用于测试各种加密算法
 * @author dx911
 *
 *	MD5	SHA1
	
	* 可逆算法
		BASE64
		HEX
 */
public class EncrptTest {

	//测试MD5
	@Test
	public void testMD5()
	{
		String plainPsd = "123456";
		String encrptPsd = DigestUtils.md5Hex(plainPsd.getBytes());
		System.out.println(encrptPsd);
	}
	
	//测试SHA1
	@Test
	public void testSHA1()
	{
		String plainPsd = "123456";
		String encrptPsd = DigestUtils.sha1Hex(plainPsd.getBytes());
		System.out.println(encrptPsd);
	}
	
	//测试BASE64加密
	@Test
	public void testBASE64Encode()
	{
		String plainPsd = "123456";
		String encrptPsd = Base64.encodeBase64String(plainPsd.getBytes());
		System.out.println(encrptPsd);
	}
	
	//测试BASE64解密
	@Test
	public void testBASE64Decode()
	{
		String Encrpted = "MTIzNDU2";
		String dencrptPsd = new String(Base64.decodeBase64(Encrpted.getBytes()));
		System.out.println(dencrptPsd);
	}
	
	//测试HEX加密
	@Test
	public void testHexEncode()
	{
		String plainPsd = "123456";
		String encrptPsd = Hex.encodeHexString(plainPsd.getBytes());
		System.out.println(encrptPsd);
	}
	
	//测试HEX解密
	@Test
	public void testHexDecode() throws DecoderException
	{
		String Encrpted = "313233343536";
		String dencrptPsd = new String(Hex.decodeHex(Encrpted.toCharArray()));
		System.out.println(dencrptPsd);
	}
	
	//plainPsd = 123456
	/* 1:生成一个随机数
	 * 2：用可逆算法加密随机数
	 * 3 将随机数和我们的密码用不可逆加密算法(sha1) 进行加密
	 * 4：将第三不得到的字符串值用可逆的加密算加密
	 * 5 将第二步和第四步的值拼凑成我们需要的加密值
	 * 
	 */
	public static final String plainPassword = "123456";
	public static final String DEFAULT_URL_ENCODING = "UTF-8";
	public static final String SHA1 = "SHA-1";
	public static final String MD5 = "MD5";
	
	public static final int iteration = 1024;
	
	//c848e4daa330bfcbd46affabfaa5084901cb851977e4c93ace849ced
	@Test
	public void testOAencrypt()
	{
		//1
		byte[] random = EncryptUtil.generateSalt(8);
		//2
		String randomHex = Hex.encodeHexString(random);
		//3
		byte[] thirdEncrypt = EncryptUtil.sha1(plainPassword.getBytes(), random, 1024);
		//byte[] thirdEncrypt = EncryptUtil.sha1(new String(randomHex + plainPassword).getBytes());
		//4
		String MainEncrypted = Hex.encodeHexString(thirdEncrypt);
		//String finalEncrypted = randomHex + MainEncrypted;
		
		System.out.println(randomHex + MainEncrypted);
	}
	
	@Test
	public void testPsdValidator() throws DecoderException
	{
		String encryptedPsd = "c848e4daa330bfcbd46affabfaa5084901cb851977e4c93ace849ced";
		String randomHex = encryptedPsd.substring(0, 16);
		byte[] salt =  Hex.decodeHex(randomHex.toCharArray());
		byte[] thirdEncrypt = EncryptUtil.sha1(plainPassword.getBytes(), salt, 1024);
		String MainEncrypted = Hex.encodeHexString(thirdEncrypt);
		//String finalEncrypted = randomHex + MainEncrypted;
		
		System.out.println(randomHex + MainEncrypted);
	}
}
