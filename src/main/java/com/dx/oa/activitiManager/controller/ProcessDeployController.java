package com.dx.oa.activitiManager.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.repository.Deployment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dx.oa.activitiManager.service.IWorkFlowService;

/**
 * 流程部署增删改功能
 * @author dx911
 *
 */
@Controller
@RequestMapping("/activitimgr/processDeploy")
public class ProcessDeployController {

	private static Logger logger = Logger.getLogger(ProcessDeployController.class);
	
	@Autowired 
	private IWorkFlowService workFlowService;
	
	@RequestMapping("/gotoProcessDeployList")
	public String gotoProcessDeployList(Model model)
	{
		List<Deployment> deploymentList = new ArrayList<Deployment>();
		deploymentList = workFlowService.getDeployments();
		model.addAttribute("deploymentList", deploymentList);
		return "activitimanage/processDeploy/processDeployList";
		//return "activitimanage/processDeploy/processDeployAdd";
	}
	
	@RequestMapping("/gotoDeployEdit")
	public String gotoDeployEdit(Model model)
	{
		return "activitimanage/processDeploy/processDeployAdd";
	}
	
	/**
	 * 1.判断上传的文件是否为空
	 * 2.判断上传文件的后缀，给出提示
	 * 3.调用接口完成流程的部署
	 */
	@RequestMapping("/addProcessDeploy")
	public String addProcessDeploy(HttpServletRequest request,MultipartFile file,
			Model model)
	{
		if(file != null)
		{	
			String originalFileName = file.getOriginalFilename();
			String filename = (String)request.getParameter("name");
			if(originalFileName.indexOf("zip") > 0)
			{
				workFlowService.addDeployment(file, filename);
				return "redirect:/activitimgr/processDeploy/gotoProcessDeployList";
			}
			else
			{
				model.addAttribute("processAddErrorMsg", "压缩文件只能是zip格式");
				return "activitimanage/processDeploy/processDeployAdd";
			}	
		}
		else{
			model.addAttribute("processAddErrorMsg", "请检查上传文件内容");
			return "activitimanage/processDeploy/processDeployAdd";
		}
	}
	
	@RequestMapping("/delProcessDeploy")
	public @ResponseBody Map<String,Object> delProcessDeploy(String deploymentId)
	{
		 Map<String,Object> resultMap =  new HashMap<String,Object>();
		 if(workFlowService.delDeploymentById(deploymentId,true))
		 {
			 resultMap.put("result", "删除成功");
		 }
		 else
		 {
			 resultMap.put("result", "删除失败");
		 }
		 return resultMap;
	}
}
