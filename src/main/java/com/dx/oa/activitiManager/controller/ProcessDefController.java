package com.dx.oa.activitiManager.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.repository.ProcessDefinition;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dx.oa.activitiManager.service.IWorkFlowService;

/**
 * 流程定义的查看
 * @author dx911
 *
 */
@Controller
@RequestMapping("/activitimgr/processDefinition")
public class ProcessDefController {

	private static Logger logger = Logger.getLogger(ProcessDefController.class);
	
	@Autowired 
	private IWorkFlowService workFlowService;
	
	@RequestMapping("/gotoProcessDefinitionList")
	public String gotoProcessDefList(Model model)
	{
		List<ProcessDefinition> processDefList = workFlowService.getProcessDefList();
		model.addAttribute("processDefinitionList", processDefList);
		return "activitimanage/processDefinition/processDefinitionList";
	}
	
	@RequestMapping("/gotoProcessDefinitionImage")
	public String gotoProcessDefinitionImage(@ModelAttribute("deploymentId") String deploymentId,
			@ModelAttribute("imageName") String imageName)
	{
		
		return "activitimanage/processDefinition/processDefinitionImage";
	}
	
	/**
	 * 获取流程图,用response.getOutputStream获取
	 * 进入流程图片查看页面
	 * img src可以使用outputstream返回结果
	 * @param deploymentId
	 * @param imageName
	 * @param response
	 * 
	 */
	@RequestMapping("/getProcessDefinitionImage")
	public void getProcessDefinitionImage(String deploymentId,String imageName,HttpServletResponse response)
	{
		InputStream imageStream = workFlowService.getProcessDefImageStream(deploymentId, imageName);
		response.setContentType("img/png");
		response.setCharacterEncoding("utf-8");
		try {
			ServletOutputStream sos =  response.getOutputStream();
			int len = 0;
			byte[] buffer = new byte[1024];
			while((len = imageStream.read(buffer, 0, 1024)) > 0)
			{
				sos.write(buffer, 0, len);
			}
			sos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
