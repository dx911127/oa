package com.dx.oa.activitiManager.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dx.oa.activitiManager.service.IWorkFlowService;
import com.dx.oa.framework.util.UserUtils;
import com.dx.oa.sysmng.service.IUserService;

/**
 * 流程部署增删改功能
 * @author dx911
 *
 */
@Controller
@RequestMapping("/activitimgr/processTask")
public class ProcessTaskController {

	private static Logger logger = Logger.getLogger(ProcessTaskController.class);
	
	@Autowired 
	private IWorkFlowService workFlowService;
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping("/gotoProcessTaskList")
	public String gotoProcessTaskList(Model model)
	{
		String assignee = String.valueOf(UserUtils.getCurrentUserId());
		//assignee = null;
		List<Task> taskList = workFlowService.getTaskList(assignee);
		model.addAttribute("taskList", taskList);
		return "activitimanage/processTask/processTaskList";
	}
	
	@RequestMapping("/gotoProcessTaskDetail")
	public String gotoProcessTaskDetail(String taskId,Model model)
	{
		if(taskId == null)
		{
			return "redirect:/activitimgr/processTask/gotoProcessTaskList";
		}
		String taskDetailUrl =  this.workFlowService.getTaskFormKeyByTaskId(taskId);
		//System.out.println("url" + taskDetailUrl);
		if(taskDetailUrl == null)
		{
			return "redirect:/activitimgr/leaveProcess/gotoLeaveProcessTaskDetail?taskId="+taskId;
		}
		return "redirect:" + taskDetailUrl + "?taskId=" + taskId;
		//return "redirect:/activitimgr/leaveProcess/gotoLeaveProcessTaskDetail?taskId="+taskId;
	}
	
	@RequestMapping("/completeTask")
	public @ResponseBody Map<String,Object> completeTask(String taskId,String outcome,String commentMsg)
	{
		Map<String,Object> resMap = new HashMap<String,Object>();
		try
		{
			this.workFlowService.completeTask(taskId, outcome, commentMsg);
			resMap.put("result", "完成任务成功");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resMap.put("result", "完成任务失败");
		}
		return resMap;
		
	}
}
