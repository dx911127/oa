package com.dx.oa.activitiManager.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Comment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dx.oa.activitiManager.bean.LeaveBean;
import com.dx.oa.activitiManager.service.ILeaveBeanService;
import com.dx.oa.activitiManager.service.IWorkFlowService;
import com.dx.oa.framework.util.UserUtils;
import com.dx.oa.sysmng.service.IUserService;
import com.github.pagehelper.util.StringUtil;

/**
 * 
 * 类描述：springmvc的第一个程序 登陆验证controller  
 * 类名称：com.tz.springmvc.sysmanage.controller.LoginController       
 * 创建人：keven  
 * 创建时间：2016年12月13日 下午5:37:03
 * @version  V1.0
 * /activitimgr/leaveProces/gotoLeaveProcessList
 */
@Controller
@RequestMapping("/activitimgr/leaveProcess")
public class LeaveProcessController {
	
	private static Logger logger = Logger.getLogger(LeaveProcessController.class);
	
	@Autowired
	private ILeaveBeanService leaveBeanService;
	
	@Autowired
	private IWorkFlowService workFlowService;
	 
	@Autowired
	private IUserService userService;

	 
	@RequestMapping("/gotoLeaveProcessList")
	public String gotoLeaveProcessList(){
		return "activitimanage/leaveProcess/leaveProcessList";
	}
	
	@RequestMapping("/gotoLeaveProcessEdit")
	public String gotoLeaveProcessEdit(@ModelAttribute("editFlag") int editFlag,Long leaveId,Model model){
		
		//进入修改页面需要将用户明细对象查询出来返回给页面显示
		if(editFlag==2){
			LeaveBean leaveBean = this.leaveBeanService.getLeaveBeanById(leaveId);
			leaveBean.setLeaveDate(new Date());
			model.addAttribute("leaveBean",leaveBean);
		}
		return "activitimanage/leaveProcess/leaveProcessEdit";
	}

	 
	 
	@RequestMapping("/getLeaveProcessList")
	public @ResponseBody Map<String,Object> getLeaveProcessList(){
		//返回对象
		Map<String,Object> resultMap = new HashMap<String,Object>();	
		List<LeaveBean> leaveBeanList = new ArrayList<LeaveBean>();
		LeaveBean leaveBean = new LeaveBean();
		leaveBean.setLeaveUserId(new Long(UserUtils.getCurrentUserId()));
		leaveBeanList = this.leaveBeanService.getLeaveBeanList(leaveBean);
//		if(leaveBeanList.size() > 0)
//		System.out.println(leaveBeanList.get(0));
		resultMap.put("leaveBeanList", leaveBeanList);
		return resultMap;
	}
	
	 
	@RequestMapping("/saveLeaveProcess") 
	public @ResponseBody Map<String,Object> saveLeaveProcess(@RequestBody LeaveBean leaveBean){
	//	System.out.println(leaveBean);
		Map<String,Object> resultMap = new HashMap<String,Object>();	
		
		try{
			if(leaveBean.getLeaveId()!=null){//修改 
				leaveBeanService.updateLeaveBean(leaveBean);
				resultMap.put("result","修改请假单成功");
			}else{
				leaveBeanService.addLeaveBean(leaveBean);
				resultMap.put("result","增加请假单成功");
			}
	  		 
		}catch(Exception e){
			logger.error("操作请假单失败",e);
			resultMap.put("result","操作请假单失败");
		}
		return resultMap;
	} 
		
	@RequestMapping("/delLeaveProcess") 
	public @ResponseBody Map<String,Object> delLeaveProcess(Long leaveId){
		Map<String,Object> resultMap = new HashMap<String,Object>();	
		try{		
			if(leaveBeanService.delLeaveBean(leaveId))
				resultMap.put("result","删除请假单成功");
		}catch(Exception e){
			logger.error("删除请假单失败",e);
			resultMap.put("result","删除请假单失败");
		}	  	 	
		return resultMap;
	}
	
	
	@RequestMapping("/doLeaveProcess") 
	public @ResponseBody Map<String,Object> doLeaveProcess(Long leaveId){
		Map<String,Object> resultMap = new HashMap<String,Object>();	
		try{		
			leaveBeanService.doLeaveProcess(leaveId);
			resultMap.put("result","申请请假成功,请转往任务处理功能提交请假单");

		}catch(Exception e){
			logger.error("申请请假失败",e);
			resultMap.put("result","申请请假失败");

		}
		return resultMap;
	}
	
	@RequestMapping("/gotoLeaveProcessTaskDetail")
	public String gotoLeaveProcessTaskDetail(@ModelAttribute("taskId") String taskId,
			Model model)
	{
		/**
		 * 使用任务id查询相关的某个流程定义的表单(taskId,leaveId)
		 * 加入一个批注输入框
		 * 根据任务id动态地生成下一步的操作按钮
		 * 查询历史的审批信息(act_hi_comment)
		 * 	 
		 */
		LeaveBean leaveBean = leaveBeanService.getLeaveBeanByTaskId(taskId);
		
		int userId = new Integer(String.valueOf(leaveBean.getLeaveUserId()));
		leaveBean.setLeaveUserName(userService.getUserById(userId).getUserName());
		model.addAttribute("leaveBean", leaveBean);
		
		List<PvmTransition>  pvmtranList = this.workFlowService.getOutComeListByTaskId(taskId);
		
		List<String> buttonNameList = new ArrayList<String>();
		if(pvmtranList != null)
		{
			for(PvmTransition pvm:pvmtranList)
			{
				String outcomeName = (String) pvm.getProperty("name");
				if(StringUtil.isNotEmpty(outcomeName))
				{
					buttonNameList.add(outcomeName);
				}
				else
				{
					buttonNameList.add("确认提交");
				}
			}
		}
		model.addAttribute("buttonNameList", buttonNameList);
		List<Comment> commentList = workFlowService.getCommentListByTaskId(taskId);
		model.addAttribute("commentList", commentList);
		return "activitimanage/leaveProcess/leaveProcssTaskDetail";
	}
	
	@RequestMapping("/gotoLeaveProcessImage")
	public String gotoLeaveProcessImage(String processInstanceId,Model model)
	{
		System.out.println(processInstanceId);
		if(processInstanceId != null)
		{
			String deploymentId = workFlowService.getDeploymentIdByPID(processInstanceId);
			String imageName = workFlowService.getImageNameByPId(processInstanceId);
			
			model.addAttribute("deploymentId",deploymentId);
			model.addAttribute("imageName", imageName);
			
			ActivityImpl activitImpl = this.workFlowService.getActivitiCoordinate(processInstanceId);
			model.addAttribute("activitImpl", activitImpl);
		}
		return "activitimanage/leaveProcess/leaveProcessImage";
	}
	 
}
