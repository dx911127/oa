package com.dx.oa.activitiManager.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.springframework.web.multipart.MultipartFile;

/**
 * 流程的相关操作
 * @author dx911
 *
 */
public interface IWorkFlowService {
	
	/**
	 * 获取所有的流程部署
	 * @return
	 */
	public List<Deployment> getDeployments();

	public Deployment addDeployment(MultipartFile file, String name);

	public boolean delDeploymentById(String deploymentId,boolean flag);

	/**
	 * 获取所有流程定义信息
	 * @return
	 */
	public List<ProcessDefinition> getProcessDefList();
	
	/**
	 * 根据部署id和流程图的名称获取流程图的流
	 * @param deploymentId
	 * @param imageName
	 * @return
	 */
	public InputStream getProcessDefImageStream(String deploymentId,String imageName);
	
	/**
	 * 根据流程定义的key和业务关系key和流程变量启动流程
	 * @param leaveProcessKey
	 * @param businessKey
	 * @param variables
	 */
	public ProcessInstance startProcess(String leaveProcessKey, String businessKey, Map<String, Object> variables);

	/**
	 * 根据assignee查询任务表
	 * @param assignee
	 * @return
	 */
	public List<Task> getTaskList(String assignee);

	/**
	 * 根据任务id获取对应任务的明细页面处理url
	 * 
	 */
	public String getTaskFormKeyByTaskId(String taskId);

	/**
	 * 根据任务id获取任务对象
	 * @param taskId
	 * @return
	 */
	public Task getTaskById(String taskId);

	
	public ProcessInstance getProcessInstanceById(String pid);

	/**
	 * 根据任务id获取任务后的连线文字
	 * @param taskId
	 */
	public List<PvmTransition>  getOutComeListByTaskId(String taskId);

	/**
	 * 根据任务id获取该流程实例的所有批注信息
	 * @param taskId
	 * @return
	 */
	public List<Comment> getCommentListByTaskId(String taskId);

	/**
	 * 完成任务
	 * @param taskId
	 * @param outcome
	 * @param commentMsg
	 */
	public void completeTask(String taskId, String outcome, String commentMsg);

	/**
	 * 根据deploymentId获取deploymentId
	 * @param processInstanceId
	 * @return
	 */
	public String getDeploymentIdByPID(String processInstanceId);

	/**
	 * 
	 * @param processInstanceId
	 * @return
	 */
	public String getImageNameByPId(String processInstanceId);

	public ActivityImpl getActivitiCoordinate(String processInstanceId);
}
