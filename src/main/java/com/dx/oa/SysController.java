package com.dx.oa;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dx.oa.framework.dto.TreeDto;
import com.dx.oa.framework.util.TreeUtil;
import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.User;
import com.dx.oa.sysmng.service.IDeptService;

/**
 * 部门功能的Controller
 * @author dx911
 *
 */
@Controller
public class SysController {

	private static Logger logger = Logger.getLogger(SysController.class);


	
	/**
	 * 对于树形结构的数据,我们在删除的时候需要注意,必须保证无子节点删除
	 * @param deptId
	 * @return
	 */

	@RequestMapping("/logout")
	public String logout(HttpSession session)
	{
		session.invalidate();
		return "redirect:/gotoLogin";
	}

	
	
}
