package com.dx.oa.framework.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.dx.oa.sysmng.entity.Area;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.Menu;
import com.dx.oa.sysmng.entity.RoleToArea;
import com.dx.oa.sysmng.entity.RoleToDept;
import com.dx.oa.sysmng.entity.RoleToMenu;
/**
 * 角色的一些小功能
 * @author dx911
 *
 */
public class RoleUtil {
	
	/**
	 * 将脏数据从另一个List中删除
	 */
	public static List<RoleToMenu> getExistMenuList(List<Menu> DList,List<RoleToMenu> selList)
	{
		if(selList == null || selList.size() == 0)
			return null;
		List<RoleToMenu> existList = new ArrayList<RoleToMenu>();
		Set<Long> DSet= new HashSet<Long>();
		for(int i=0; i<DList.size(); i++)
			DSet.add(new Long(DList.get(i).getId()));
		Long roleId = selList.get(0).getRoleId();
		for(int i=0; i<selList.size(); i++)
		{
			if(DSet.contains(selList.get(i).getMenuId()))
			{
				RoleToMenu rtm = new RoleToMenu();
				rtm.setRoleId(roleId);
				rtm.setMenuId(selList.get(i).getMenuId());
				existList.add(rtm);
			}
		}
		return existList;
	}
	
	public static List<RoleToDept> getExistDeptList(List<Dept> DList,List<RoleToDept> selList)
	{
		if(selList == null || selList.size() == 0)
			return null;
		List<RoleToDept> existList = new ArrayList<RoleToDept>();
		Set<Long> DSet= new HashSet<Long>();
		for(int i=0; i<DList.size(); i++)
			DSet.add(new Long(DList.get(i).getId()));
		Long roleId = selList.get(0).getRoleId();
		for(int i=0; i<selList.size(); i++)
		{
			if(DSet.contains(selList.get(i).getDeptId()))
			{
				RoleToDept rtd = new RoleToDept();
				rtd.setRoleId(roleId);
				rtd.setDeptId(selList.get(i).getDeptId());
				existList.add(rtd);
			}
		}
		return existList;
	}
	
	public static List<RoleToArea> getExistAreaList(List<Area> DList,List<RoleToArea> selList)
	{
		if(selList == null || selList.size() == 0)
			return null;
		List<RoleToArea> existList = new ArrayList<RoleToArea>();
		Set<Long> DSet= new HashSet<Long>();
		for(int i=0; i<DList.size(); i++)
			DSet.add(new Long(DList.get(i).getId()));
		Long roleId = selList.get(0).getRoleId();
		for(int i=0; i<selList.size(); i++)
		{
			
			if(DSet.contains(selList.get(i).getAreaId()))
			{
				RoleToArea rta = new RoleToArea();
				rta.setRoleId(roleId);
				rta.setAreaId(selList.get(i).getAreaId());
				existList.add(rta);
			}
		}
		return existList;
	}
}
