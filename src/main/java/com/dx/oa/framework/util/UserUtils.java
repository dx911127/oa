package com.dx.oa.framework.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dx.oa.sysmng.entity.User;
import com.dx.oa.sysmng.service.impl.UserService;

/**
 * 获取当前登录用户信息的工具类
 * @author dx911
 *
 */
@Component
public class UserUtils {
	
	private static UserService userService;
	
	@Autowired(required = true)
	public static void setUserService(UserService userService) {
		UserUtils.userService = userService;
	}

	
	
	public static Integer getCurrentUserId()
	{
		ServletRequestAttributes hsq = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		User tuser = (User)hsq.getRequest().getSession().getAttribute("user");
		if(tuser != null)
		{
			return tuser.getUserId();
		}
		else
			return 1;
	}
	
	public static String getCurrentUserName()
	{
		ServletRequestAttributes hsq = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		User tuser = (User)hsq.getRequest().getSession().getAttribute("user");
		return tuser.getUserName();
	}
	
	public static List<Long> getCurRoleList()
	{
		
		ServletRequestAttributes hsq = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		User tuser = (User)hsq.getRequest().getSession().getAttribute("user");

		if(tuser != null)
		{
			return userService.getRoleListByUserId(tuser.getUserId());
					//tuser.getUserId();
		}
		else
			return null;
	}
}
