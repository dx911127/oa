package com.dx.oa.framework.dto;

import com.dx.oa.sysmng.entity.Menu;

public class TreeDto implements Comparable<TreeDto>{
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private Integer id;
	private Integer parentId;
	private String name;
	
	public int compareTo(TreeDto arg0) {
		// TODO Auto-generated method stub
		if(arg0.getId() == null || id == null)
			return 0;
		return id.compareTo(arg0.getId());
	}	
}
