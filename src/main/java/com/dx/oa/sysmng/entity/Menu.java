package com.dx.oa.sysmng.entity;

import java.sql.Timestamp;

import com.dx.oa.framework.dto.TreeDto;

public class Menu extends TreeDto implements java.io.Serializable
{
	@Override
	public String toString() {
		return "[parentName:" + parentName + ", sort:" + sort + ", href:" + href + ", target:" + target + ", icon:"
				+ icon + ", isShow:" + isShow + ", permission:" + permission + ", updateBy:" + updateBy
				+ ", updateDate:" + updateDate + ", remarks:" + remarks + ", delFlag:" + delFlag + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3409507393396024746L;
	
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public char getIsShow() {
		return isShow;
	}
	public void setIsShow(char isShow) {
		this.isShow = isShow;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(char delFlag) {
		this.delFlag = delFlag;
	}
	
	private String parentName;
	private Integer sort;
	private String href;
	private String target;
	private String icon;
	private char isShow;
	private String permission;
	private String updateBy;
	private Timestamp updateDate;
	private String remarks;
	private char delFlag;
}
