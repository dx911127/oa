package com.dx.oa.sysmng.entity;

import java.sql.Timestamp;

public class Role implements java.io.Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2497933031567477804L;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(char delFlag) {
		this.delFlag = delFlag;
	}

	private Long id;
	private String name;
	private String updateBy;
	private Timestamp updateDate;
	private String remarks;
	private char delFlag;
	
	@Override
	public String toString() {
		return "[id:" + id + ", name:" + name + ", updateBy:" + updateBy + ", updateDate:" + updateDate + ", remarks:"
				+ remarks + ", delFlag:" + delFlag + "]";
	}
}
