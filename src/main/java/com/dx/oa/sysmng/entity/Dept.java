package com.dx.oa.sysmng.entity;

import java.sql.Timestamp;

import com.dx.oa.framework.dto.TreeDto;

public class Dept extends TreeDto implements java.io.Serializable
{
	private static final long serialVersionUID = 7172436510188475914L;
	/**
	 * 
	 */
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(char delFlag) {
		this.delFlag = delFlag;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	private Integer sort;
	private String code;
	private String address;
	private String master;
	private String phone;
	private String fax;
	private String email;
	private String updateBy;
	private Timestamp updateDate;
	private String parentName;
	private String remarks;
	private char delFlag;
	@Override
	public String toString() {
		return "[sort:" + sort + ", code:" + code + ", address:" + address + ", master:" + master + ", phone:" + phone
				+ ", fax:" + fax + ", email:" + email + ", updateBy:" + updateBy + ", updateDate:" + updateDate
				+ ", parentName:" + parentName + ", remarks:" + remarks + ", delFlag:" + delFlag + "]";
	}	
	
}
