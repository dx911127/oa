package com.dx.oa.sysmng.entity;

public class RoleToArea implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -974440458724418582L;
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getAreaId() {
		return areaId;
	}
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}
	
	private Long roleId;
	private Long areaId;
}
