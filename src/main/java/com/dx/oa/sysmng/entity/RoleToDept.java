package com.dx.oa.sysmng.entity;

public class RoleToDept implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5275404307671535433L;
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	
	private Long roleId;
	private Long deptId;
}
