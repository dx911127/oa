package com.dx.oa.sysmng.entity;

import java.sql.Timestamp;

public class Dict implements java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -650743961016665635L;
	/**
	 * 
	 */
	
	@Override
	public String toString() {
		return "[id:" + id + ", value:" + value + ", label:" + label + ", type:" + type + ", description:" + description
				+ ", sort:" + sort + ", parentId:" + parentId + ", updateBy:" + updateBy + ", updateDate:" + updateDate
				+ ", remarks:" + remarks + ", delFlag:" + delFlag + "]";
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(char delFlag) {
		this.delFlag = delFlag;
	}
	
	private Integer id;
	private String value;
	private String label;
	private String type;
	private String description;
	private Integer sort;
	private String parentId;
	private String updateBy;
	private Timestamp updateDate;
	private String remarks;
	private char delFlag;
}
