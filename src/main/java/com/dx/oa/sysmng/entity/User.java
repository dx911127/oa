package com.dx.oa.sysmng.entity;

import java.sql.Timestamp;

public class User implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 818803304585805305L;

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(char delFlag) {
		this.delFlag = delFlag;
	}

	private Integer userId;
	private Integer deptId;
	private String loginName;
	private String password;
	private String userNo;
	private String userName;
	private String email;
	private String phone;
	private String mobile;
	private String updateBy;
	private Timestamp updateDate;
	private String remarks;
	private char delFlag;
	
	@Override
	public String toString() {
		return "[userId:" + userId + ", deptId:" + deptId + ", loginName:" + loginName + ", password:" + password
				+ ", userNo:" + userNo + ", userName:" + userName + ", email:" + email + ", phone:" + phone
				+ ", mobile:" + mobile + ", updateBy:" + updateBy + ", updateDate:" + updateDate + ", remarks:"
				+ remarks + ", delFlag:" + delFlag + "]";
	}
}
