package com.dx.oa.sysmng.dto;
import java.util.List;
import java.util.Map;

import com.dx.oa.sysmng.entity.Role;

public class RoleDto implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8036122624657985000L;
	/**
	 * 
	 */
	
	private Role role;
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public List<Long> getDeptIds() {
		return deptIds;
	}
	public void setDeptIds(List<Long> deptIds) {
		this.deptIds = deptIds;
	}
	public List<Long> getMenuIds() {
		return menuIds;
	}
	public void setMenuIds(List<Long> menuIds) {
		this.menuIds = menuIds;
	}
	public List<Long> getAreaIds() {
		return areaIds;
	}
	public void setAreaIds(List<Long> areaIds) {
		this.areaIds = areaIds;
	}
	private List<Long> deptIds;
	private List<Long> menuIds;
	private List<Long> areaIds;
}
