package com.dx.oa.sysmng.dto;

import java.io.Serializable;

import com.dx.oa.sysmng.entity.User;

public class UserDto extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2918642509146774405L;
	private String deptName;

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	} 
}
