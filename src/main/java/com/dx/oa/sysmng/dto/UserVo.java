package com.dx.oa.sysmng.dto;

import java.util.Map;

import com.dx.oa.sysmng.entity.User;

public class UserVo implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2058378405363384926L;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Map<Long, Long> getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(Map<Long, Long> roleIds) {
		this.roleIds = roleIds;
	}
	
	private User user;
	Map<Long,Long> roleIds;
}
