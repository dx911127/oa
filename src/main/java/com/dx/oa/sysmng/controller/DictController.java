package com.dx.oa.sysmng.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dx.oa.framework.util.PageUtils;
import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.User;
import com.dx.oa.sysmng.service.IDictService;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping("/sysmgr/dict")
public class DictController {
	
	@Autowired
	private IDictService dictService;

	private static Logger logger = Logger.getLogger(DictController.class);

	/**
	 * 进入字典查询功能
	 * @return
	 */
	@RequestMapping("/gotoDictList")
	public String gotoDictList(Model model)
	{
		model.addAttribute("dictTypeList", dictService.getAllDictType());
		return "sysmanage/dict/dictList";
	}
	
	//进入字典编辑功能
	@RequestMapping("/gotoDictEdit")
	public String gotoDictEdit(Integer dictId,@ModelAttribute("editFlag") Integer editFlag,Model model)
	{	
		if(editFlag == 2)
			model.addAttribute("dict", dictService.getDictById(dictId));
		else
		{
			if(dictId != null)
			{
				Dict ndict = dictService.getDictById(dictId);
				ndict.setId(null);
				model.addAttribute("dict", ndict);
			}
		}
		return "sysmanage/dict/dictEdit";
	}
	
	//查询字典信息
	@RequestMapping("/getDictListPage")
	public @ResponseBody Map<String,Object> getDictListPage(String type, String description, Integer pageNo, Integer pageSize)
	{
		//System.out.println("type:" + type);
		//System.out.println("description:" + description);
		//pageSize = 9;
		Map<String,Object> responseMap = new HashMap<String,Object>();  
		if(StringUtils.isEmpty(type))
			type = null;
		//responseMap.put("dictList", dictService.searchDict(type, description));
		PageInfo<Dict> pageInfo = dictService.getDictListPage(type, description, pageNo, pageSize);
		responseMap.put("dictList", pageInfo.getList());
		String pageStr = PageUtils.pageStr(pageInfo, "dictMgr.getDictListPage");
		responseMap.put("pageStr",pageStr);
		return responseMap;
	}

	
	//删除字典
	@RequestMapping("/delDict")
	public @ResponseBody Map<String,String> delDict(int dictId)
	{
		Map<String,String> responseMap = new HashMap<String,String>();
		if(dictService.delDictById(dictId) > 0)
			responseMap.put("result", "删除成功");
		else
			responseMap.put("result", "删除失败");
		return responseMap;
	}

	//保存修改
	@RequestMapping("/saveDict")
	public @ResponseBody Map<String,String> saveDict(Dict mdict,HttpSession session)
	{
		Map<String,String> responseMap = new HashMap<String,String>();
		mdict.setUpdateBy(((User)session.getAttribute("user")).getUserName());
		//System.out.println("mdict:"+ mdict.toString());
		if(mdict == null)
		{
			responseMap.put("result", "操作失败");
			return responseMap;
		}
		if(mdict.getId() == null)
		{
			if(dictService.insertDict(mdict) > 0)
				responseMap.put("result", "插入成功");
			else
				responseMap.put("result", "插入失败");
		}
		else
		{
			if(dictService.updateDict(mdict) > 0)
				responseMap.put("result","更新成功");
			else
				responseMap.put("result", "更新失败");
		}
		return responseMap;
	}
}
