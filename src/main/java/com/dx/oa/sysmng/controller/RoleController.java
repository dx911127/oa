package com.dx.oa.sysmng.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dx.oa.framework.dto.TreeDto;
import com.dx.oa.framework.util.RoleUtil;
import com.dx.oa.framework.util.TreeUtil;
import com.dx.oa.sysmng.dto.RoleDto;
import com.dx.oa.sysmng.entity.Area;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Menu;
import com.dx.oa.sysmng.entity.Role;
import com.dx.oa.sysmng.entity.User;
import com.dx.oa.sysmng.service.IAreaService;
import com.dx.oa.sysmng.service.IDeptService;
import com.dx.oa.sysmng.service.IMenuService;
import com.dx.oa.sysmng.service.IRoleService;

/**
 * 角色管理
 * @author dx911
 *
 */
@Controller
@RequestMapping("/sysmgr/role")
public class RoleController {
	
	@Autowired
	private IRoleService roleService;

	@Autowired
	private IMenuService menuService;
	
	@Autowired
	private IDeptService deptService;
	
	@Autowired
	private IAreaService areaService;
	
	private static Logger logger = Logger.getLogger(RoleController.class);

	/**
	 * 进入字典查询功能
	 * @return
	 */
	
	@RequestMapping("/gotoRoleList")
	public String gotoRoleList(Model model)
	{
		List<Role> roleList = roleService.getRoleList();
		model.addAttribute("roleList", roleList);
		return "sysmanage/role/roleList";				
	}
	
	@RequestMapping("/gotoRoleEdit")
	public String gotoRoleEdit(@ModelAttribute("editFlag") int editFlag, Long roleId, Model model)
	{
		//需要将role菜单区域部门三棵树显示出来
		List<Menu> menuList = menuService.getMenuList();
		
		List<Dept> deptList = deptService.getDeptList();
		
		List<Area> areaList = areaService.getAreaList();
		
		model.addAttribute("menuList", menuList);
		model.addAttribute("deptList", deptList);
		model.addAttribute("areaList", areaList);
		if(2 == editFlag)
		{
			if(roleId != null)
			{
				try
				{
					model.addAttribute("roleMenuList", RoleUtil.getExistMenuList(menuList, roleService.getRMListById(roleId)));
					model.addAttribute("roleDeptList", RoleUtil.getExistDeptList(deptList, roleService.getRDListById(roleId)));
					model.addAttribute("roleAreaList", RoleUtil.getExistAreaList(areaList, roleService.getRAListById(roleId)));
					model.addAttribute("role", roleService.getRoleById(roleId));
				}catch(Exception e)
				{
					logger.error("角色显示有问题", e);
				}
			}
		}

		return "sysmanage/role/roleEdit";
	}
	
	@RequestMapping("/saveRole")
	public @ResponseBody Map<String,Object> saveRole(@RequestBody RoleDto roleDto)
	{
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		//判断新增或是修改,根据role对象是否有ID
		try
		{
			if(roleDto!=null && roleDto.getRole()!=null)
			{
				if(roleDto.getRole().getId() != null)
				{
					if(roleService.update(roleDto))
						resultMap.put("result", "更新角色信息成功");
					else
						resultMap.put("result", "更新角色信息失败");
				}
				else
				{
					if(roleService.add(roleDto))
						resultMap.put("result", "增加角色信息成功");
					else
						resultMap.put("result", "增加角色信息失败");
				}
			}
			else
			{
				logger.error("操作角色信息失败");
			}
		}
		catch(Exception e){
			logger.error("操作角色信息失败",e);
		}
		
		return resultMap;
	}
	
	@RequestMapping("/delRole")
	public @ResponseBody Map<String,Object> delRole(Long roleId)
	{
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try
		{
			if(roleId != null)
			{
				if(roleService.deleteRoleById(roleId))
					resultMap.put("result", "删除成功");
				else
					resultMap.put("result", "删除失败");
			}
			else
			{
				logger.error("操作角色信息失败");
			}
		}
		catch(Exception e){
			logger.error("操作角色信息失败",e);
		}
		return resultMap;
		
	}
}
