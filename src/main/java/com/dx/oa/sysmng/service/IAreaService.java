package com.dx.oa.sysmng.service;

import java.util.List;

import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Area;

/**
 * 部门管理的业务层接口
 * @author dx911
 *
 */
public interface IAreaService {
	public List<Area> getAreaList();

	public int deleteAreaById(Integer areaId);
	
	public int getChildCnt(Integer areaId);

	public Area getAreaById(Integer areaId);

	public int insertArea(Area area);

	public int updateArea(Area area);

	public List<Area> getAreaListByUserId(Integer currentUserId);
}
