package com.dx.oa.sysmng.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dx.oa.framework.util.UserUtils;
import com.dx.oa.sysmng.dto.RoleDto;
import com.dx.oa.sysmng.entity.Role;
import com.dx.oa.sysmng.entity.RoleToArea;
import com.dx.oa.sysmng.entity.RoleToDept;
import com.dx.oa.sysmng.entity.RoleToMenu;
import com.dx.oa.sysmng.mapper.RoleMapper;
import com.dx.oa.sysmng.service.IRoleService;

@Service
public class RoleService implements IRoleService{

	@Autowired
	private RoleMapper roleMapper;
	
	public List<Role> getRoleList() {
		// TODO Auto-generated method stub
		return roleMapper.getRoleList();
	}

	/*
	 * 1.保存角色信息,返回角色id
	 * 2.根据角色id，部门id组合角色部门对应列表，进行批量插入
	 * (non-Javadoc)
	 * @see com.dx.oa.sysmng.service.IRoleService#add(com.dx.oa.sysmng.dto.RoleDto)
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public boolean add(RoleDto roleDto) {
		// TODO Auto-generated method stub
		//System.out.println("--addddddddddddddddddddddddddddddddd--");
		boolean flag = true;
		Role role = roleDto.getRole();
		role.setUpdateBy(UserUtils.getCurrentUserId().toString());
		role.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		role.setDelFlag('0');
		int row = roleMapper.addRole(role);
		if(0 == row)
			flag = false;
		
		Long roleId = role.getId();
		
		
//		List<RoleToMenu> roleMenuList = new ArrayList<RoleToMenu>();
//		for(Long menuId:roleDto.getMenuIds().keySet())
//		{
//			RoleToMenu rtm = new RoleToMenu();
//			rtm.setMenuId(menuId);
//			rtm.setRoleId(roleId);
//			roleMenuList.add(rtm);
//		}		
		//批量插入菜单角色对应表

		
//		List<RoleToDept> roleDeptList = new ArrayList<RoleToDept>();
//		for(Long deptId:roleDto.getDeptIds().keySet())
//		{
//			RoleToDept rtd = new RoleToDept();
//			rtd.setDeptId(deptId);
//			rtd.setRoleId(roleId);
//			roleDeptList.add(rtd);
//		}
//		//批量插入部门角色对应表
//		if(roleDeptList.size() > 0)
//			row = roleMapper.addRoleToDeptBatch(roleDeptList);
//		if(0 == row)
//			flag = false;
//		
//		List<RoleToArea> roleAreaList = new ArrayList<RoleToArea>();
//		for(Long areaId:roleDto.getAreaIds().keySet())
//		{
//			RoleToArea rta = new RoleToArea();
//			rta.setAreaId(areaId);
//			rta.setRoleId(roleId);
//			roleAreaList.add(rta);
//		}
//		
//		//批量插入菜单角色对应表
//		if(roleAreaList.size()>0)
//			row = roleMapper.addRoleToAreaBatch(roleAreaList);
//		if(0 == row)
//			flag = false;
		
		
		if(roleDto.getMenuIds().size() > 0)
		{
			Map<String,Object> paraMap = new HashMap<String,Object>();
			paraMap.put("menulist", roleDto.getMenuIds());
			paraMap.put("roleId", roleId);
			row = roleMapper.addRoleToMenuBatch(paraMap);
			if(0 == row)
				flag = false;	
		}

		
		if(roleDto.getDeptIds().size() > 0)
		{
			Map<String,Object> paraMap = new HashMap<String,Object>();
			paraMap.put("deptlist", roleDto.getDeptIds());
			paraMap.put("roleId", roleId);
			row = roleMapper.addRoleToDeptBatch(paraMap);
			if(0 == row)
				flag = false;
		}

		
		if(roleDto.getAreaIds().size() > 0)
		{
			Map<String,Object> paraMap = new HashMap<String,Object>();
			paraMap.put("arealist", roleDto.getAreaIds());
			paraMap.put("roleId", roleId);
			row = roleMapper.addRoleToAreaBatch(paraMap);
			if(0 == row)
				flag = false;
		}

		
		return flag;
	}

	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public boolean deleteRoleById(Long roleId) {
		// TODO Auto-generated method stub
		boolean flag = true;
		int row = roleMapper.delRoleToMenu(roleId);
		if(row < 0)
			flag = false;
		row = roleMapper.delRoleToDept(roleId);
		if(row < 0)
			flag = false;
		row = roleMapper.delRoleToArea(roleId);
		if(row < 0)
			flag = false;
		row = roleMapper.delRoleById(roleId);
		if(row <= 0)
			flag = false;
		return flag;
		
	}
	
	/**
	 * 保存对应角色信息
	 * 删除id对应的角色外键
	 * 加入角色id对应的新的外键
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public boolean update(RoleDto roleDto) {
		// TODO Auto-generated method stub
		boolean flag = true;
		Role role = roleDto.getRole();
		role.setUpdateBy(UserUtils.getCurrentUserId().toString());
		role.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		role.setDelFlag('0');

		int row = roleMapper.updateRole(role);
		if(row < 0)
			flag = false;
		
		/**
		 * 删除角色id映射表中内容
		 */
		Long roleId = role.getId();
		row = roleMapper.delRoleToMenu(roleId);
		if(row < 0)
			flag = false;
		row = roleMapper.delRoleToDept(roleId);
		if(row < 0)
			flag = false;
		row = roleMapper.delRoleToArea(roleId);
		if(row < 0)
			flag = false;
		
		if(roleDto.getMenuIds().size() > 0)
		{
			Map<String,Object> paraMap = new HashMap<String,Object>();
			paraMap.put("menulist", roleDto.getMenuIds());
			paraMap.put("roleId", roleId);
			row = roleMapper.addRoleToMenuBatch(paraMap);
			if(0 == row)
				flag = false;	
		}

		
		if(roleDto.getDeptIds().size() > 0)
		{
			Map<String,Object> paraMap = new HashMap<String,Object>();
			paraMap.put("deptlist", roleDto.getDeptIds());
			paraMap.put("roleId", roleId);
			row = roleMapper.addRoleToDeptBatch(paraMap);
			if(0 == row)
				flag = false;
		}

		
		if(roleDto.getAreaIds().size() > 0)
		{
			Map<String,Object> paraMap = new HashMap<String,Object>();
			paraMap.put("arealist", roleDto.getAreaIds());
			paraMap.put("roleId", roleId);
			row = roleMapper.addRoleToAreaBatch(paraMap);
			if(0 == row)
				flag = false;
		}
		
		return flag;
	}
	
	public List<RoleToMenu> getRMListById(Long roleId) {
		// TODO Auto-generated method stub
		return roleMapper.getRoleMenuById(roleId);
	}

	public List<RoleToDept> getRDListById(Long roleId) {
		// TODO Auto-generated method stub
		return roleMapper.getRoleDeptById(roleId);
	}

	public List<RoleToArea> getRAListById(Long roleId) {
		// TODO Auto-generated method stub
		return roleMapper.getRoleAreaById(roleId);
	}

	public Role getRoleById(Long roleId) {
		// TODO Auto-generated method stub
		return roleMapper.getRoleById(roleId);
	}



}
