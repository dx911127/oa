package com.dx.oa.sysmng.service;

import java.util.List;

import com.dx.oa.sysmng.dto.RoleDto;
import com.dx.oa.sysmng.entity.Role;
import com.dx.oa.sysmng.entity.RoleToArea;
import com.dx.oa.sysmng.entity.RoleToDept;
import com.dx.oa.sysmng.entity.RoleToMenu;

/**
 * 角色管理的业务层接口
 * @author dx911
 *
 */
public interface IRoleService {

	List<Role> getRoleList();

	boolean add(RoleDto roleDto);

	boolean deleteRoleById(Long roleId);

	List<RoleToMenu> getRMListById(Long roleId);

	List<RoleToDept> getRDListById(Long roleId);
	
	List<RoleToArea> getRAListById(Long roleId);

	Role getRoleById(Long roleId);

	/**
	 * 修改角色信息
	 * @param roleDto
	 * @return
	 */
	boolean update(RoleDto roleDto);
	
}
