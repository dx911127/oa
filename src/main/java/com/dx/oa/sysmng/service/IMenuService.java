package com.dx.oa.sysmng.service;

import java.util.List;

import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Menu;

/**
 * 菜单管理的业务层接口
 * @author dx911
 *
 */
public interface IMenuService {
	public List<Menu> getMenuList();

	public int deleteMenuById(Integer menuId);
	
	public int getChildCnt(Integer menuId);

	public Menu getMenuById(Integer menuId);

	public int insertMenu(Menu menu);

	public int updateMenu(Menu menu);

	public List<Menu> getMenuListById(Integer userId);
}
