package com.dx.oa.sysmng.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dx.oa.framework.util.EncryptUtil;
import com.dx.oa.framework.util.UserUtils;
import com.dx.oa.sysmng.dto.UserDto;
import com.dx.oa.sysmng.entity.Area;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Area;
import com.dx.oa.sysmng.entity.User;
import com.dx.oa.sysmng.mapper.AreaMapper;
import com.dx.oa.sysmng.mapper.RoleMapper;
import com.dx.oa.sysmng.mapper.AreaMapper;
import com.dx.oa.sysmng.mapper.UserMapper;
import com.dx.oa.sysmng.service.IAreaService;
import com.dx.oa.sysmng.service.IUserService;

@Service
public class AreaService implements IAreaService {

	@Autowired
	private AreaMapper areaMapper;

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private RoleMapper roleMapper;
	
	@Autowired
	private UserMapper userMapper;
	
	public List<Area> getAreaListByUserId(Integer currentUserId) {
		// TODO Auto-generated method stub
		return areaMapper.getAreaListByUserId(currentUserId);
	}

	public List<Area> getAreaList() {
		// TODO Auto-generated method stub
		return areaMapper.getAreaList();
	}

	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public int deleteAreaById(Integer areaId) {
		// TODO Auto-generated method stub
		int row = roleMapper.deleteAreaRoleByAreaId(areaId);
		if(row < 0)
			return 0;
		return areaMapper.deleteAreaById(areaId);
	}

	public int getChildCnt(Integer areaId) {
		// TODO Auto-generated method stub
		return areaMapper.getChildCnt(areaId);
	}

	public Area getAreaById(Integer areaId) {
		// TODO Auto-generated method stub
		return areaMapper.getAreaById(areaId);
	}

	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public int insertArea(Area area) {
		// TODO Auto-generated method stub
		User tuser = (User)httpSession.getAttribute("user");
		if(tuser != null)
			area.setCreateBy(UserUtils.getCurrentUserId().toString());
		area.setCreateDate(new Timestamp(System.currentTimeMillis()));
		int row = areaMapper.insertArea(area);
		if(row < 1)
			return 0;
		Map<String,Object> para  = new HashMap<String,Object>();
		para.put("roleId", 1);
		para.put("arealist", new Integer[]{area.getId()});
		row = roleMapper.addRoleToAreaBatch(para);
		int nowid = UserUtils.getCurrentUserId();
		try {
			Long trid = userMapper.getRoleListByUserId(nowid).get(0);
			if(trid != null && trid != 1)
			{
				para.put("roleId", trid);
				row = roleMapper.addRoleToAreaBatch(para);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			if(row < 1)
				return 0;
			return 1;
		}
	}

	public int updateArea(Area area) {
		// TODO Auto-generated method stub
		return areaMapper.updateArea(area);
	}





}
