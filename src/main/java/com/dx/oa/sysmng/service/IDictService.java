package com.dx.oa.sysmng.service;

import java.util.List;

import com.dx.oa.sysmng.entity.Dict;
import com.github.pagehelper.PageInfo;

/**
 * 字典相关业务层接口
 * @author dx911
 *
 */
public interface IDictService {
	/**
	 * 获取所有字典类型
	 * @return
	 */
	public List<String> getAllDictType();
	public List<Dict> searchDict(String type,String description);
	public PageInfo<Dict> getDictListPage(String type,String description,int pageNo,int pageSize);
	public int delDictById(int dictId);
	public int updateDict(Dict mdict);
	public int insertDict(Dict mdict);
	public Dict getDictById(int dictId);
}
