package com.dx.oa.sysmng.service;

import java.util.List;

import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Dept;

/**
 * 部门管理的业务层接口
 * @author dx911
 *
 */
public interface IDeptService {
	public List<Dept> getDeptList();

	public int deleteDeptById(Integer deptId);
	
	public int getChildCnt(Integer deptId);

	public Dept getDeptById(Integer deptId);

	public int insertDept(Dept dept);

	public int updateDept(Dept dept);

	public List<Dept> getDeptListByUserId(Integer userId);
}
