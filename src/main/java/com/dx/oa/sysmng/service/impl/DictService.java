package com.dx.oa.sysmng.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dx.oa.framework.util.EncryptUtil;
import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.mapper.DictMapper;

import com.dx.oa.sysmng.service.IDictService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class DictService implements IDictService {

	@Autowired
	private DictMapper dictMapper;
	
	
	public List<String> getAllDictType() {
		// TODO Auto-generated method stub
		return dictMapper.getAllDictType();
	}
	
	public List<Dict> searchDict(String type, String description) {
		// TODO Auto-generated method stub
		//return null;
		Dict dict = new Dict();
		dict.setType(type);
		dict.setDescription(description);
		return dictMapper.getDictByDict(dict);
	}

	public int delDictById(int dictId) {
		// TODO Auto-generated method stub
		return dictMapper.delDictById(dictId) ;
	}

	public int updateDict(Dict mdict) {
		// TODO Auto-generated method stub
		mdict.setParentId("0");
		mdict.setDelFlag('0');
		return dictMapper.updateDict(mdict);
	}

	public int insertDict(Dict mdict) {
		// TODO Auto-generated method stub
		mdict.setParentId("0");
		mdict.setDelFlag('0');
		return dictMapper.insertDict(mdict);
	}

	public Dict getDictById(int dictId) {
		// TODO Auto-generated method stub
		return dictMapper.getDictById(dictId);
	}

	public PageInfo<Dict> getDictListPage(String type, String description, int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		PageHelper.startPage(pageNo, pageSize);
		Dict dict = new Dict();
		dict.setType(type);
		dict.setDescription(description);
		List<Dict> dictList = dictMapper.getDictByDict(dict);
		PageInfo<Dict> pageInfo = new PageInfo<Dict>(dictList);
		return pageInfo;
	}




}
