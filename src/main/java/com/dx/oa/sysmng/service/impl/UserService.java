package com.dx.oa.sysmng.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dx.oa.framework.util.EncryptUtil;
import com.dx.oa.framework.util.UserUtils;
import com.dx.oa.sysmng.dto.UserDto;
import com.dx.oa.sysmng.dto.UserVo;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.User;
import com.dx.oa.sysmng.mapper.DeptMapper;
import com.dx.oa.sysmng.mapper.UserMapper;
import com.dx.oa.sysmng.service.IUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private DeptMapper deptMapper;

//	public UserMapper getUserMapper() {
//		return userMapper;
//	}
//
//
//	public void setUserMapper(UserMapper userMapper) {
//		this.userMapper = userMapper;
//	}

	public boolean validate(String loginname, String password) {
		// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			if(userMapper.validateUser(loginname, password) != null)
				return true;
			return false;
	}


	public User validateByUser(String loginname, String password) throws DecoderException{
		// TODO Auto-generated method stub
		User nuser = new User();
		nuser.setLoginName(loginname);
		System.out.println(nuser.getLoginName());
		List<User> userList = userMapper.getUserList(nuser);
		System.out.println(userList.size());
		if(userList == null || userList.size() == 0)
		{
			return null;
		}
		else
		{
			String databasePwd = userList.get(0).getPassword();
			//System.out.println(databasePwd);
			String inputPwd = EncryptUtil.transformPsd(password, databasePwd);
			System.out.println(inputPwd);
			if(inputPwd == null)
				return null;
			if(inputPwd.equals(databasePwd))
				return userList.get(0);
			else
				return null;
		}
			
			
	}


	public int updatePwdById(String pwd, int userId) {

		return userMapper.updatePwdById(pwd,userId);
		
	}


	public User getUserById(int userId) {
		// TODO Auto-generated method stub
		
		User nuser = userMapper.getUserById(userId);
		if(nuser == null)
			return null;
		
		if(nuser.getUserNo() == null)
			nuser.setUserNo("");
		
		if(nuser.getEmail() == null)
			nuser.setEmail("");
		
		if(nuser.getPhone() == null)
			nuser.setPhone("");
		
		if(nuser.getMobile() == null)
			nuser.setMobile("");
		
		if(nuser.getRemarks() == null)
			nuser.setRemarks("");
		return nuser;
	}


	public Dept getDeptById(int deptId) {
		// TODO Auto-generated method stub
		return deptMapper.getDeptById(deptId);
	}


	public int updateUserInfo(User nowuser) {
		// TODO Auto-generated method stub
		if(nowuser.getUserName().equals(""))
			nowuser.setUserName(null);
		if(nowuser.getPhone().equals(""))
			nowuser.setPhone(null);
		if(nowuser.getMobile().equals(""))
			nowuser.setMobile(null);
		if(nowuser.getEmail().equals(""))
			nowuser.setEmail(null);
		if(nowuser.getRemarks().equals(""))
			nowuser.setRemarks(null);
		return userMapper.updateUserInfo(nowuser);
	}


	public UserDto getUserDtoById(int userId) {
		// TODO Auto-generated method stub
		return userMapper.getUserDtoById(userId);
	}


	public PageInfo<UserDto> getUserDtoList(User nuser, Integer pageNo, Integer pageSize) {
		// TODO Auto-generated method stub
		//System.out.println("-----" + Thread.currentThread().getStackTrace()[1].getMethodName() + "-----");
		PageHelper.startPage(pageNo,pageSize);
		List<UserDto> userList = userMapper.getUserDtoList(nuser);
		PageInfo<UserDto> pud = new PageInfo<UserDto>(userList);
		return pud;
		
	}

	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public boolean addUser(UserVo userVo) {
		// TODO Auto-generated method stub
		//填充用户更新者和更新时间
		boolean flag = true;
		User nowUser = userVo.getUser();
		nowUser.setUpdateBy(UserUtils.getCurrentUserId().toString());
		nowUser.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		nowUser.setPassword(EncryptUtil.encryptPwd("123"));
		int row = userMapper.insertUser(nowUser);
			
		if(row < 1)
			flag = false;
		
		Map<String,Object> userRoleMap = new HashMap<String,Object>();
		userRoleMap.put("userId", nowUser.getUserId());
		userRoleMap.put("roleList", userVo.getRoleIds().keySet().toArray());
		row = userMapper.insertUserRoleBatch(userRoleMap);
		if(row < 0)
			flag = false;
		return flag;
	}


	public boolean delUser(Integer userId) {
		// TODO Auto-generated method stub
		boolean flag = true;
		int row = userMapper.delUserRole(userId);
		if(row < 0)
			flag = false;
		row = userMapper.delUser(userId);
		if(row < 1)
			flag = false;

		return flag;
	}


	public List<Long> getRoleListByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return userMapper.getRoleListByUserId(userId);
	}

	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public boolean updateUser(UserVo userVo) {
		// TODO Auto-generated method stub
		boolean flag = true;
		
		User nuser = userVo.getUser();
		nuser.setUpdateBy(UserUtils.getCurrentUserId().toString());
		nuser.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		
		int row = userMapper.delUserRole(nuser.getUserId());
		if(row < 0)
			flag = false;
		
		Map<String,Object> userRoleMap = new HashMap<String,Object>();
		userRoleMap.put("userId", nuser.getUserId());
		userRoleMap.put("roleList", userVo.getRoleIds().keySet().toArray());
		row = userMapper.insertUserRoleBatch(userRoleMap);
		
		if(row < 0)
			flag = false;
		
		row = userMapper.updateUserInfo(nuser);
		if(row < 0)
			flag = false;
		return flag;
	}


	public List<String> getRoleListStrByUserId(Integer currentUserId) {
		// TODO Auto-generated method stub
		return userMapper.getRoleListStrByUserId(currentUserId);
	}


}
