package com.dx.oa.sysmng.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dx.oa.framework.util.UserUtils;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.Menu;
import com.dx.oa.sysmng.mapper.DeptMapper;
import com.dx.oa.sysmng.mapper.RoleMapper;
import com.dx.oa.sysmng.mapper.UserMapper;
import com.dx.oa.sysmng.service.IDeptService;

@Service
public class DeptService implements IDeptService {

	@Autowired
	private DeptMapper deptMapper;

	@Autowired
	private RoleMapper roleMapper; 
	
	@Autowired
	private UserMapper userMapper;
	
	public List<Dept> getDeptList() {
		// TODO Auto-generated method stub
		return deptMapper.getDeptList();
	}

	public List<Dept> getDeptListByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return deptMapper.getDeptListByUserId(userId);
	}
	
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public int deleteDeptById(Integer deptId) {
		// TODO Auto-generated method stub
		int row = roleMapper.deleteDeptRoleByDeptId(deptId);
		if(row < 0)
			return 0;
		return deptMapper.deleteDeptById(deptId);
	}

	public int getChildCnt(Integer deptId) {
		// TODO Auto-generated method stub
		return deptMapper.getChildCnt(deptId);
	}

	public Dept getDeptById(Integer deptId) {
		// TODO Auto-generated method stub
		return deptMapper.getDeptById(deptId);
	}

	/**
	 * 加入与超管的映射
	 */
	@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED)
	public int insertDept(Dept dept) {
		// TODO Auto-generated method stub
		int row = deptMapper.insertDept(dept);
		if(row < 1)
			return 0;
		Map<String,Object> para  = new HashMap<String,Object>();
		para.put("roleId", 1);
		para.put("deptlist", new Integer[]{dept.getId()});
		row = roleMapper.addRoleToDeptBatch(para);
		int nowid = UserUtils.getCurrentUserId();
		try {
			Long trid = userMapper.getRoleListByUserId(nowid).get(0);
			if(trid != null && trid != 1)
			{
				para.put("roleId", trid);
				row = roleMapper.addRoleToDeptBatch(para);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			if(row < 1)
				return 0;
			return 1;
		}
	}

	public int updateDept(Dept dept) {
		// TODO Auto-generated method stub
		return deptMapper.updateDept(dept);
	}






}
