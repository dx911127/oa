package com.dx.oa.sysmng.service;

import java.util.List;

import org.apache.commons.codec.DecoderException;

import com.dx.oa.sysmng.dto.UserDto;
import com.dx.oa.sysmng.dto.UserVo;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.User;
import com.github.pagehelper.PageInfo;

/**
 * 用户服务
 * @author dx911
 *
 */
public interface IUserService {
	public boolean validate(String loginname,String password);
	public User validateByUser(String loginname,String password) throws DecoderException;
	public int updatePwdById(String newPassword, int userId);
	public User getUserById(int userId);
	public Dept getDeptById(int deptId);
	public int updateUserInfo(User nowuser);
	public UserDto getUserDtoById(int userId);
	
	/**
	 * 按条件分页查询用户列表
	 * @param nuser
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public PageInfo<UserDto> getUserDtoList(User nuser, Integer pageNo, Integer pageSize);
	/**
	 * 新增用户
	 * @param userVo
	 * @return
	 */
	public boolean addUser(UserVo userVo);
	/**
	 * 删除用户
	 * @param userId
	 * @return
	 */
	public boolean delUser(Integer userId);
	/**
	 * 根据用户id获取用户角色列表
	 * @param userId
	 * @return
	 */
	public List<Long> getRoleListByUserId(Integer userId);
	/**
	 * 更新用户
	 * @param userVo
	 * @return
	 */
	public boolean updateUser(UserVo userVo);
	
	/**
	 * 获取角色对应用户名
	 * @param currentUserId
	 * @return
	 */
	public List<String> getRoleListStrByUserId(Integer currentUserId);
}
