package com.dx.oa.sysmng.mapper;

import java.util.List;

import com.dx.oa.sysmng.entity.Area;
import com.dx.oa.sysmng.entity.User;

public interface AreaMapper {
	Area getAreaById(int areaId);

	List<Area> getAreaList();

	int deleteAreaById(Integer areaId);

	int getChildCnt(Integer areaId);

	int insertArea(Area area);

	int updateArea(Area area);

	List<Area> getAreaListByUserId(Integer currentUserId);
}
