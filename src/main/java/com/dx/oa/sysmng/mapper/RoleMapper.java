package com.dx.oa.sysmng.mapper;

import java.util.List;
import java.util.Map;

import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Menu;
import com.dx.oa.sysmng.entity.Role;
import com.dx.oa.sysmng.entity.RoleToArea;
import com.dx.oa.sysmng.entity.RoleToDept;
import com.dx.oa.sysmng.entity.RoleToMenu;

public interface RoleMapper {

	List<Role> getRoleList();

	/**
	 * 增加角色对象
	 * @param role
	 * @return
	 */
	int addRole(Role role);

	/**
	 * 批量插入角色菜单信息
	 * @param roleMenuList
	 * @return
	 */
	int addRoleToMenuBatch(Map<String,Object> para);

	/**
	 * 批量插入角色部门信息
	 * @param roleDeptList
	 * @return
	 */
	int addRoleToDeptBatch(Map<String,Object> para);

	/**
	 * 批量插入角色角色区域信息
	 * @param roleAreaList
	 * @return
	 */
	int addRoleToAreaBatch(Map<String,Object> para);

	/**
	 * 根据角色id删除角色菜单表
	 * @param roleId
	 * @return
	 */
	int delRoleToMenu(Long roleId);

	/**
	 * 根据角色id删除角色部门表
	 * @param roleId
	 * @return
	 */
	int delRoleToDept(Long roleId);

	/**
	 * 根据角色id删除角色区域表
	 * @param roleId
	 * @return
	 */
	int delRoleToArea(Long roleId);

	/**
	 * 根据角色id删除角色本身
	 * @param roleId
	 * @return
	 */
	int delRoleById(Long roleId);

	/**
	 * 根据角色id查询角色菜单对应表
	 * @param roleId
	 * @return
	 */
	List<RoleToMenu> getRoleMenuById(Long roleId);

	/**
	 * 根据角色id查询角色部门对应表
	 * @param roleId
	 * @return
	 */
	List<RoleToDept> getRoleDeptById(Long roleId);

	/**
	 * 根据角色id查询角色区域对应表
	 * @param roleId
	 * @return
	 */
	List<RoleToArea> getRoleAreaById(Long roleId);

	/**
	 * 根据角色Id获取角色信息
	 * @param roleId
	 * @return
	 */
	Role getRoleById(Long roleId);

	/**
	 * 更新角色信息
	 * @param role
	 * @return
	 */
	int updateRole(Role role);

	/**
	 * 在菜单角色映射表中根据菜单id删除相应映射
	 * @param menuId
	 * @return
	 */
	int deleteMenuRoleByMenuId(Integer menuId);
	
	/**
	 * 在部门角色映射表中根据菜单id删除相应映射
	 * @param menuId
	 * @return
	 */
	int deleteDeptRoleByDeptId(Integer menuId);
	
	/**
	 * 在区域角色映射表中根据菜单id删除相应映射
	 * @param menuId
	 * @return
	 */
	int deleteAreaRoleByAreaId(Integer menuId);
	
	/**
	 * 根据用户id删除用户区域对应表
	 */
	int deleteUserRoleByUserId(Integer userId);
}
