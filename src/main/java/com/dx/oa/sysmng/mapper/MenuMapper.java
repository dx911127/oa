package com.dx.oa.sysmng.mapper;

import java.util.List;

import com.dx.oa.sysmng.entity.Dict;
import com.dx.oa.sysmng.entity.Menu;

public interface MenuMapper {
	/**
	 * 获取所有菜单
	 * @return
	 */
	List<Menu> getMenuList();

	/**
	 * 删除菜单
	 * @param menuId
	 * @return
	 */
	int deleteMenuById(Integer menuId);

	int getChildCnt(Integer menuId);

	String getNameById(Integer menuId);
	
	Menu getMenuById(Integer menuId);

	int insertMenu(Menu menu);

	int updateMenu(Menu menu);

	List<Menu> getMenuListByUserId(Integer userId);
	
}
