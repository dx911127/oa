package com.dx.oa.sysmng.mapper;

import java.util.List;

import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.User;

public interface DeptMapper {
	Dept getDeptById(int deptId);

	List<Dept> getDeptList();

	int deleteDeptById(Integer deptId);

	int getChildCnt(Integer deptId);

	int insertDept(Dept dept);

	int updateDept(Dept dept);

	List<Dept> getDeptListByUserId(Integer userId);
}
