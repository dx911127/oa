package com.dx.oa.sysmng.mapper;

import java.util.List;

import com.dx.oa.sysmng.entity.Dict;

/**
 * 字典增删改查接口
 * @author dx911
 *
 */
public interface DictMapper {

	//del_flag标志位用于实现逻辑删除
	List<String> getAllDictType();
	
	List<Dict> getDictByDict(Dict dict);

	int delDictById(int dictId);

	int logDelById(int dictId);
	
	int updateDict(Dict mdict);

	int insertDict(Dict mdict);

	Dict getDictById(int dictId);
	
	
	
}
