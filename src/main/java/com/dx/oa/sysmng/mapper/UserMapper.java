package com.dx.oa.sysmng.mapper;

import java.util.List;
import java.util.Map;

import com.dx.oa.sysmng.dto.UserDto;
import com.dx.oa.sysmng.entity.Dept;
import com.dx.oa.sysmng.entity.User;

public interface UserMapper {
	Integer validateUser(String loginname,String password);
	/**
	 * 根据User对象查询用户列表
	 * @param user
	 * @return
	 */
	List<User> getUserList(User user);
	int updatePwdById(String pwd, int userId);
	User getUserById(int userId);
	int updateUserInfo(User nuser);
	public UserDto getUserDtoById(int userId);
	List<UserDto> getUserDtoList(User nuser);
	int insertUser(User nowUser);
	int insertUserRoleBatch(Map<String, Object> userRoleMap);
	int delUser(Integer userId);
	int delUserRole(Integer userId);
	List<Long> getRoleListByUserId(Integer userId);
	List<String> getRoleListStrByUserId(Integer currentUserId);
}
